## Universal Self-Service Password Reset

The Universal Self-Service Password Reset project (aka USSPR) permits Windows users to reset their password themselves in an enterprise environment, without relying on the helpdesk. They only need a corporate workstation that belongs to the domain, from which the USSPR can be deployed on member systems.

### Overview

USSPR consists in 2 components:
- The Credential Provider (i.e. "UssprCP.dll") is copied into %SystemRoot%\System32 during installation. It permits to display the USSPR tile on the login and lockout screens.
- The Fat Client (i.e. "BrowserApp.exe") is installed into USSPR directory, along with its configuration file [i.e. "UssprBrowserApp.exe.config"] and the Apache log4net library [i.e. "log4net.dll"].

The Fat Client gets executed by the Credential Provider and instantiates a WebBrowser control through the Windows Presentation Foundation graphical subsystem. The WebBrowser control is then used to load the URL of the page offering the password reset feature.

### GIT branches

There are currently 4 branches for that project:
- The master branch tracks the latest stable version for both Windows 10 and Windows 7 that was relesed.
- The develop branch track the developments over the latest stable version.
- The experimental branch tracks experiments with native image compiler and AppContainer.
- The legacy branch tracks the latest development version of the Windows 7 edition and is therefore outdated.

Git tags are used for released versions. The latest tag is “WindowsClientv1.0.0.10” and it is valid for both Windows 10 and Windows 7.

### Prerequisites

It should be noted that:
- USSPR can be installed on Windows 10 or Windows 7. It requires Internet Explorer 11 and .NET Framework v4.6.1 or superior.
- USSPR should not be installed on an existing version since upgrade is not supported. Please first uninstall any existing version of USSPR before installing the new one:
   - Go to Start -> Windows System -> Control Panel.
   - Click on "Uninstall a program" in the "Programs" area.
   - Select any existing version of USSPR which starts with "Usspr" (old versions) or "Universal Self-Service" (newer versions).
   - Click on "Uninstall".  
- USSPR should be installed through the batch, which first need to be customized according to the targeted environment:
   - Please use an absolute path for your parameters (like with the log path and the XML configuration file). This is required because the installer does not run directly from the directory from where it gets executed.
   - Run the batch as admin.  
- It is also necessary to adapt the XML configuration file which is used by the MSI installer called by the batch. The only mandatory parameter in this XML file is the “UssprUrl” field, which permits to define the URL opened by the BrowserApp.
- Visual Studio, Windows SDK and the WiX Toolset build tools are required to compile the binary in case of source code modification.
- To Enable Certificate Public Key Pinning, recompile from source using the right public key in hexadecimal format: set the value of PUB_KEY in the source file UriCertificateCheckLoader.cs

Please read the User Guide in the Wiki section for further information.

### License

This project is licensed under the [GNU General Public License v3.0](https://choosealicense.com/licenses/gpl-3.0/).