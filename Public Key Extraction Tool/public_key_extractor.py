from optparse import OptionParser
import logging
import sys
import socket, ssl
import OpenSSL
import binascii

# For Linux:
#from crypto.PublicKey import RSA
#from crypto.Util import asn1
# For Windows:
import crypto

# Public key extraction query
def ExtractPubKey(target, format_key):
    try:
        context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        ssl_sock = context.wrap_socket(s, server_hostname=target)
        ssl_sock.settimeout(5)
        ssl_sock.connect((target, 443))
        ssl_sock.close()
        cert = ssl.get_server_certificate((target, 443))
        x509 = OpenSSL.crypto.load_certificate(OpenSSL.crypto.FILETYPE_PEM, cert)
        if format_key == "PEM":
            PubKey = OpenSSL.crypto.dump_publickey(OpenSSL.crypto.FILETYPE_PEM,x509.get_pubkey())
            return PubKey
        elif format_key == "DER_HEX":
            PubKeyDER = OpenSSL.crypto.dump_publickey(OpenSSL.crypto.FILETYPE_ASN1,x509.get_pubkey())
            return binascii.hexlify(PubKeyDER)

    except IOError:
        print("  [-] Error: No route to host")
        sys.exit(2)
    except (KeyboardInterrupt, SystemExit):
        sys.exit(0)
    except:
        error = sys.exc_info()[0]
        print("  [-] Error: %s" % error)
        sys.exit(3)

# Main function with options for running script directly
def main():
    # Setup the command line arguments.
    optp = OptionParser()

    # Output verbosity options
    optp.add_option('-q', '--quiet', help='set logging to ERROR',
                  action='store_const', dest='loglevel',
                  const=logging.ERROR, default=logging.INFO)
    optp.add_option('-d', '--debug', help='set logging to DEBUG',
                  action='store_const', dest='loglevel',
                  const=logging.DEBUG, default=logging.INFO)
    optp.add_option('-v', '--verbose', help='set logging to COMM',
                  action='store_const', dest='loglevel',
                  const=5, default=logging.INFO)

    # Target option for the public key extraction query
    optp.add_option("-t", "--target", dest="target",
                  help="Target to extract the public key from")
                  
    opts, args = optp.parse_args()

    print """
    _/_/_/    _/    _/  _/_/_/_/  _/_/_/_/_/   
   _/    _/  _/  _/    _/            _/        
  _/_/_/    _/_/      _/_/_/        _/         
 _/        _/  _/    _/            _/          
_/        _/    _/  _/_/_/_/      _/"""     
    print "\n  Public Key Extraction Tool v0.1\n"
    
    try:
        # Prompt the user if he didn't specify any target
        if opts.target is None:
            opts.target = raw_input("[+] URL for public key extraction: ")
            if not opts.target.strip():
                print "[+] Using 'www.smartwavesa.com' as default target"
                opts.target = "www.smartwavesa.com"
            if ((opts.target.strip().lower().startswith("http://")) or (opts.target.strip().lower().startswith("https://"))):
                print "[+] Invalid target provided"
                sys.exit(1)
        else:
            if (not opts.target.strip()) or (opts.target.strip().lower().startswith("http://")) or (opts.target.strip().lower().startswith("https://")):
                print "[+] Invalid target provided"
                sys.exit(1)
    except (KeyboardInterrupt, SystemExit):
        sys.exit(0)

    print "[+] Grabbing certificate from " + opts.target
    PEM_PubKey = ExtractPubKey(opts.target, "PEM")
    print "[+] Extracted public key in PEM format\n"
    print PEM_PubKey
    DERHEX_PubKey = ExtractPubKey(opts.target, "DER_HEX")
    print "[+] Extracted public key in hexadecimal DER [ASN1 format]\n"
    print DERHEX_PubKey + "\n"

if __name__ == '__main__':
    main()

