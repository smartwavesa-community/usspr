﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ComputeNetAppHashIntoCppSource
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                using (StreamWriter outputFile = new StreamWriter(@"..\UssprCP\browserapphash.h", false))
                {
                    outputFile.WriteLine(String.Format(@"const char * browserapphash = ""{0}"";", GetBrowserAppHash()));
                }
            }
            catch (Exception e)
            {
                Console.Out.WriteLine(e.Message);
                throw e;
            }
        }

        private static string GetBrowserAppHash()
        {
            using (FileStream fs = new FileStream(@"..\UssprBrowserApp\bin\Release\UssprBrowserApp.exe", FileMode.Open))
            using (BufferedStream bs = new BufferedStream(fs))
            {
                using (SHA256Managed sha256 = new SHA256Managed())
                {
                    byte[] hash = sha256.ComputeHash(bs);
                    StringBuilder formatted = new StringBuilder(2 * hash.Length);
                    foreach (byte b in hash)
                    {
                        formatted.AppendFormat("{0:X2}", b);
                    }
                    return formatted.ToString().ToLower();
                }
            }
        }
    }
}
