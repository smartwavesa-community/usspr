﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;

namespace Usspr.BrowserApp.Helpers
{
    /// <summary>
    /// TODO: override default behaviors to avoid there commands.
    /// </summary>
    public class CommandsRemoveHelper : EventHelper<ExecutedRoutedEventArgs>
    {
        public CommandsRemoveHelper(UIElement elem)
            : base(elem)
        {
            this.HandledCommands = new List<RoutedUICommand> 
            {
                ApplicationCommands.CancelPrint, ApplicationCommands.ContextMenu, ApplicationCommands.CorrectionList,
                ApplicationCommands.Help, ApplicationCommands.New, ApplicationCommands.Open,
                ApplicationCommands.Print, ApplicationCommands.PrintPreview, ApplicationCommands.Save,
                ApplicationCommands.SaveAs
            };
        }

        public override void HandleEvent(object sender, ExecutedRoutedEventArgs e)
        {
            e.Handled = true;
        }


        /// <summary>
        /// Actually removes all bindings linked to forbidden commands
        /// </summary>
        public override void AddHandlers()
        {
            List<CommandBinding> cbToRemove = new List<CommandBinding>();
            //CanExecuteRoutedEventHandler cereh = new CanExecuteRoutedEventHandler(delegate(Object sender, CanExecuteRoutedEventArgs e) { });
            foreach (CommandBinding cb in this.TargetUIElement.CommandBindings)
            {
                if (this.HandledCommands.Any(com => (cb.Command == com)))
                {
                    cbToRemove.Add(cb);
                }
            }
            cbToRemove.ForEach(ctr => this.TargetUIElement.CommandBindings.Remove(ctr));

            this.HandledCommands.ForEach(h => this.TargetUIElement.CommandBindings.Add(new CommandBinding(h, new ExecutedRoutedEventHandler(this.HandleEvent),
                new CanExecuteRoutedEventHandler(delegate(Object sender, CanExecuteRoutedEventArgs e) { e.CanExecute = false; e.Handled = true; }))));
        }
    }
}
