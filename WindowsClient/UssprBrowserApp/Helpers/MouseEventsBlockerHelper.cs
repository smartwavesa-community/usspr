﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;

namespace Usspr.BrowserApp.Helpers
{
    /// <summary>
    /// Provide handler and event list to block shortcuts that involve a mouse events.
    /// </summary>
    public class MouseEventsBlockerHelper: EventHelper<MouseButtonEventArgs>
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(MouseEventsBlockerHelper));
        public MouseEventsBlockerHelper(UIElement elem)
            : base(elem)
        {
            log.Info("Building an instance of MouseEventsBlockerHelper");
            log.Debug("Populating list of events to handle");
            this.HandledEvents = new List<RoutedEvent> { Mouse.MouseDownEvent, Mouse.MouseUpEvent};
            log.Debug("Populated list of events to handle");
        }

        public override void HandleEvent(object sender, MouseButtonEventArgs e)
        {
            if (ModifierKeys.Any(k => Keyboard.IsKeyDown(k)))
            {
                log.Debug(string.Format("Event marked as handled: {0}", e.RoutedEvent.Name));
                e.Handled = true;
            }
        }

        private List<Key> ModifierKeys
        {
            get
            {
                return new List<Key> { Key.RightCtrl, Key.LeftCtrl, Key.RightAlt, Key.LeftAlt, Key.RightShift, Key.LeftShift, Key.LWin, Key.RWin };
            }
        }

        public override void AddHandlers()
        {
            log.Debug("Adding handlers");
            MouseButtonEventHandler meh = HandleEvent;
            base.AddHandlers(meh);
        }
    }
}
