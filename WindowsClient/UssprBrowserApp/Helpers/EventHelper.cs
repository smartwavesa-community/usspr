﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;

namespace Usspr.BrowserApp.Helpers
{
    public abstract class EventHelper<U> where U : EventArgs
    {
        public EventHelper(UIElement elem)
        {
            this.TargetUIElement = elem;
        }

        protected UIElement TargetUIElement
        { get; private set; }

        protected void AddHandlers(Delegate d)
        {
            if (HandledEvents != null)
            {
                HandledEvents.ForEach(e =>
                {
                    //HandleEventDelegate hed = HandleEvent;
                    TargetUIElement.AddHandler(e, d);
                });
            }
        }

        public abstract void AddHandlers();

        public List<RoutedEvent> HandledEvents
        {
            get;
            protected set;
        }

        public List<RoutedUICommand> HandledCommands
        {
            get;
            protected set;
        }

        protected delegate void HandleEventDelegate(object sender, U e);

        public abstract void HandleEvent(object sender, U e);
    }
}
