﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace Usspr.BrowserApp.Helpers
{
    public class KeyboardShortcutsBlockerHelper : EventHelper<KeyEventArgs>
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(KeyboardShortcutsBlockerHelper));

        private static List<Key> PressedKeys = new List<Key>();
        private const int MaxKeyCount = 3;

        /*private static List<Key>  whiteListedKey;*/
        private static Dictionary<Key, string> whiteListedKey;
        private static Dictionary<string,string> whiteListed2DCombination;
        private static Dictionary<string,string> whiteListed3DCombination;

        public KeyboardShortcutsBlockerHelper(UIElement elem)
            : base(elem)
        {
            log.Info("Building an instance of KeyboardShortcutsBlockerHelper");
            log.Debug("Populating list of events to handle");
            this.HandledEvents = new List<RoutedEvent> {Keyboard.PreviewKeyDownEvent , Keyboard.KeyDownEvent, Keyboard.PreviewKeyUpEvent, Keyboard.KeyUpEvent};
            log.Debug("Populated list of events to handle");
            log.Debug("Initializing the white liste");
            InitialiseWhiteListe();
            printWhiteListedKeys(whiteListedKey,whiteListed2DCombination,whiteListed3DCombination);
        }

        public override void HandleEvent(object sender, KeyEventArgs e)
        {
            if (e.Handled) return;

            //Check all previous keys to see if they are still pressed
            List<Key> KeysToRemove = new List<Key>();
            foreach (Key k in PressedKeys)
            {
                if (!Keyboard.IsKeyDown(k))
                    KeysToRemove.Add(k);
            }

            //Remove all not pressed keys
            foreach (Key k in KeysToRemove)
                PressedKeys.Remove(k);

            //Add the key if max count is not reached
            if (PressedKeys.Count < MaxKeyCount)
                //if (AllowedKeys.Contains(e.Key))
                if (!PressedKeys.Contains(e.Key))
                    PressedKeys.Add(e.Key);

            if (isKeySequenceAllowed(PressedKeys))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private Boolean isKeySequenceAllowed(List<Key> PK)
        {
            switch (PK.Count)
            {
                case 0:
                    return false;
                case 1:
                    return isSingleKeyAllowed(PK[0]);
                case 2:
                    return isDualKeyAllowed(PK);
                case 3:
                    return isTripleKeyAllowed(PK);
                default:
                    log.Debug(string.Format("DISALLOWED: pressing more than 3 Keys is forbidden"));
                    return false;
            }
        }

        private Boolean isSingleKeyAllowed(Key Pressed1DKeys)
        {
            //Allowing letters and numbers
            if (   (Pressed1DKeys >= Key.A && Pressed1DKeys <= Key.Z) || 
                   (Pressed1DKeys >= Key.D0 && Pressed1DKeys <= Key.D9) ||
                   (Pressed1DKeys >= Key.NumPad0 && Pressed1DKeys <= Key.NumPad9) ||
                   (Pressed1DKeys == Key.DeadCharProcessed)
               )
            {
                return true;
            }

            string test;
            if (singleWhiteListedKeys.TryGetValue(Pressed1DKeys,out test)){
            return true;
            }

            log.Debug(string.Format("DISALLOWED: Single Key: {0}", Pressed1DKeys));
            return false;
        }

        private Boolean isDualKeyAllowed(List<Key> PressedDualKeys)
        {
            string output;

            if ( !IsModifierKey(PressedDualKeys[0]) && !IsModifierKey(PressedDualKeys[1]) && isSingleKeyAllowed(PressedDualKeys[1]) ) 
            {
                return true;
            }

            //allowing shift based Uppercase letter
            if ( (PressedDualKeys[0] == Key.LeftShift || PressedDualKeys[0] == Key.RightShift) && (PressedDualKeys[1] >= Key.A && PressedDualKeys[1] <= Key.Z)  )
            {
                return true;
            }

            if (dualCombinationWhiteListedKeys.TryGetValue(GetModifierKey(PressedDualKeys[0]).ToString()+PressedDualKeys[1],out output))
            {              
                return true;
            }
            log.Debug(string.Format("DISALLOWED: Dual Key: {0} + {1} input:{2} ", PressedDualKeys[0], PressedDualKeys[1], GetModifierKey(PressedDualKeys[0]).ToString() + PressedDualKeys[1]));
            return false;
        }

        private Boolean isTripleKeyAllowed(List<Key> PressedTripleKeys)
        {
            string output;
            
            if (!IsModifierKey(PressedTripleKeys[0]) && !IsModifierKey(PressedTripleKeys[1]) && !IsModifierKey(PressedTripleKeys[2]) && isSingleKeyAllowed(PressedTripleKeys[2]) && isSingleKeyAllowed(PressedTripleKeys[1]) ) 
            {
                return true;
            }

            if (tripleCombinationWhiteListedKeys.TryGetValue(GetModifierKey(PressedTripleKeys[0]).ToString() +GetModifierKey(PressedTripleKeys[1]).ToString()+ PressedTripleKeys[2], out output))
            {
                return true;
            }
            log.Debug(string.Format("DISALLOWED: Triple Key: {0} + {1} + {2} input:{3}",PressedTripleKeys[0],PressedTripleKeys[1],PressedTripleKeys[2], GetModifierKey(PressedTripleKeys[0]).ToString() + GetModifierKey(PressedTripleKeys[1]).ToString() + PressedTripleKeys[2]));
            return false;
        }

        private ModifierKeys GetModifierKey(Key k)
        {
            if (k == Key.LeftCtrl || k == Key.RightCtrl)
                return ModifierKeys.Control;

            if (k == Key.LeftShift || k == Key.RightShift)
                return ModifierKeys.Shift;

            if (k == Key.LeftAlt || k == Key.RightAlt)
                return ModifierKeys.Alt;

            if (k == Key.LWin || k == Key.RWin)
                return ModifierKeys.Windows;

            return ModifierKeys.None;
        }
        
        private bool IsModifierKey(Key k)
        {
            if (k == Key.LeftCtrl || k == Key.RightCtrl ||
                k == Key.LeftShift || k == Key.RightShift ||
                k == Key.LeftAlt || k == Key.RightAlt ||
                k == Key.LWin || k == Key.RWin)
                return true;
            else
                return false;
        }

        /// </summary>
        private Dictionary<Key,string> singleWhiteListedKeys
        {
           get
            {
                if (whiteListedKey != null)
                {
                    InitialiseWhiteListe();
                    return whiteListedKey;
                }
                else
                {
                    return whiteListedKey;
                }
            }
        }

    private Dictionary<string,string> dualCombinationWhiteListedKeys
    {
      get
      {
                if (whiteListed2DCombination != null)
                {
                    InitialiseWhiteListe();
                    return whiteListed2DCombination;
                }
                else
                {
                    return whiteListed2DCombination;
                }
      }
    }

        private Dictionary<string,string> tripleCombinationWhiteListedKeys
        {
          get
            {
                if (whiteListed3DCombination != null)
                {
                    InitialiseWhiteListe();
                    return whiteListed3DCombination;
                }
                else
                {
                    return whiteListed3DCombination;
                }
            }
        }

        public override void AddHandlers()
        {
            log.Debug("Adding handlers");
            KeyEventHandler keh = HandleEvent;
            base.AddHandlers(keh);
        }

        private void printWhiteListedKeys(Dictionary<Key,string> white1D, Dictionary<string,string> white2D, Dictionary<string,string> white3D)
        {
            log.Debug("[Config]: White-Listed: all 36 alphanumeric characters");
            log.Debug("[Config]: White-Listed: all combination of 2 or 3 keys that does not contain a Modifier key (ctrl, shift, win, alt)");
            log.Debug($"[Config]: White-Listed {white1D.Count} Single Keys : [{string.Join(";",white1D.Keys.ToArray<Key>())}]");
            log.Debug($"[Config]: White-Listed {white2D.Count} Dual Keys : [{string.Join(";", white2D.Values.ToArray<string>())}]");
            log.Debug($"[Config]: White-Listed {white3D.Count} Triple Keys : [{string.Join(";", white3D.Values.ToArray<string>())}]");
        }

        internal void InitialiseWhiteListe()
        {
            initializeSingleWhiteListKey();
            initializeDualWhiteListedKey();
            initializeTripleWhiteListedKey();
        }

        /// <summary>
        /// In this method you declare white listed Keys.
        /// For shit, alt, windows and ctrl, there is no need to specify right and left
        /// For instance LeftCtrl will also refer to RightCtrl
        /// </summary>
        private void initializeSingleWhiteListKey()
        {
            whiteListedKey = new Dictionary<Key, string>()
            {
                {Key.Back,"" },
                {Key.Cancel,"" },
                {Key.CapsLock,"" },
                {Key.Clear,"" },
                {Key.DbeAlphanumeric,"" },
                {Key.Decimal,"" },
                {Key.Delete,"" },
                {Key.Divide,"" },
                {Key.Down,"" },
                {Key.End,"" },
                {Key.Enter,"" },
                {Key.Escape,"" },
                {Key.Insert,"" },
                {Key.Left,"" },
                {Key.Multiply,"" },
                {Key.None,"" },
                {Key.NumLock,"" },
                {Key.Oem1,"" },
                {Key.Oem102,"" },
                {Key.Oem2,"" },
                {Key.Oem3,"" },
                {Key.Oem4,"" },
                {Key.Oem5,"" },
                {Key.Oem6,"" },
                {Key.Oem7,"" },
                {Key.Oem8,"" },
                {Key.OemBackTab,"" },
                {Key.OemClear,"" },
                {Key.OemComma,"" },
                {Key.OemMinus,"" },
                {Key.OemPeriod,"" },
                {Key.OemPlus,"" },
                {Key.PageDown,"" },
                {Key.PageUp,"" },
                {Key.Right,"" },
                {Key.Scroll,"" },
                {Key.Space,"" },
                {Key.Tab,"" },
                {Key.Up,"" }
            };

        }

        /// <summary>
        /// In this method you declare white listed combination of 2 Keys.
        /// For shit, alt, windows and ctrl, there is no need to specify right and left
        /// For instance LeftCtrl will also refer to RightCtrl
        /// </summary>
        private void initializeDualWhiteListedKey()
        {
            whiteListed2DCombination = new Dictionary<string, string>()
            {
                {"Control"+Key.C,"Ctrl + C" },
                {"Control"+Key.V,"Ctrl + V" },
                {"Control"+Key.A,"Ctrl + A" },
                {"Shift"+Key.Oem1,"Shift + Oem1" },
                {"Shift"+Key.Oem102,"Shift +Oem102" },
                {"Shift"+Key.Oem2,"Shift + Oem2" },
                {"Shift"+Key.Oem3,"Shift + Oem3" },
                {"Shift"+Key.Oem4,"Shift + Oem4" },
                {"Shift"+Key.Oem5,"Shift + Oem5" },
                {"Shift"+Key.Oem6,"Shift + Oem6" },
                {"Shift"+Key.Oem7,"Shift + Oem7" },
                {"Shift"+Key.Oem8,"Shift + Oem8" },
                {"Shift"+Key.OemBackTab,"Shift + OemBackTab" },
                {"Shift"+Key.OemClear,"Shift + OemClear" },
                {"Shift"+Key.OemComma,"Shift + OemComma" },
                {"Shift"+Key.OemMinus,"Shift + OemMinus" },
                {"Shift"+Key.OemPeriod,"Shift + OemPeriod" },
                {"Shift"+Key.OemPlus,"Shift + OemPlus" },
                {"Shift"+Key.D0,"Shift + D0" },
                {"Shift"+Key.D1,"Shift + D1" },
                {"Shift"+Key.D2,"Shift + D2" },
                {"Shift"+Key.D3,"Shift + D3" },
                {"Shift"+Key.D4,"Shift + D4" },
                {"Shift"+Key.D5,"Shift + D5" },
                {"Shift"+Key.D6,"Shift + D6" },
                {"Shift"+Key.D7,"Shift + D7" },
                {"Shift"+Key.D8,"Shift + D8" },
                {"Shift"+Key.D9,"Shift + D9" }
            };
        }

        /// <summary>
        /// In this method you declare white listed combination of 3 Keys.
        /// For shit, alt, windows and ctrl, there is no need to specify right and left
        /// For instance LeftCtrl will also refer to RightCtrl
        /// </summary>
        private void initializeTripleWhiteListedKey()
        {
            whiteListed3DCombination = new Dictionary<string, string>()
            {
                {"ControlAlt"+Key.D0,"Ctrl + Alt + D0" },
                {"ControlAlt"+Key.D1,"Ctrl + Alt + D1" },
                {"ControlAlt"+Key.D2,"Ctrl + Alt + D2" },
                {"ControlAlt"+Key.D3,"Ctrl + Alt + D3" },
                {"ControlAlt"+Key.D4,"Ctrl + Alt + D4" },
                {"ControlAlt"+Key.D5,"Ctrl + Alt + D5" },
                {"ControlAlt"+Key.D6,"Ctrl + Alt + D6" },
                {"ControlAlt"+Key.D7,"Ctrl + Alt + D7" },
                {"ControlAlt"+Key.D8,"Ctrl + Alt + D8" },
                {"ControlAlt"+Key.D9,"Ctrl + Alt + D9" },
                {"ControlAlt"+Key.Oem1,"Ctrl + Alt + Oem1" },
                {"ControlAlt"+Key.Oem102,"Ctrl + Alt + Oem102" },
                {"ControlAlt"+Key.Oem2,"Ctrl + Alt + Oem2" },
                {"ControlAlt"+Key.Oem3,"Ctrl + Alt + Oem3" },
                {"ControlAlt"+Key.Oem4,"Ctrl + Alt + Oem4" },
                {"ControlAlt"+Key.Oem5,"Ctrl + Alt + Oem5" },
                {"ControlAlt"+Key.Oem6,"Ctrl + Alt + Oem6" },
                {"ControlAlt"+Key.Oem7,"Ctrl + Alt + Oem7" },
                {"ControlAlt"+Key.Oem8,"Ctrl + Alt + Oem8" },
                {"ControlAlt"+Key.OemBackTab,"Ctrl + Alt + OemBackTab" },
                {"ControlAlt"+Key.OemClear,"Ctrl + Alt + OemClear" },
                {"ControlAlt"+Key.OemComma,"Ctrl + Alt + OemClear" },
                {"ControlAlt"+Key.OemMinus,"Ctrl + Alt + OemMinus" },
                {"ControlAlt"+Key.OemPeriod,"Ctrl + Alt + OemPeriod" },
                {"ControlAlt"+Key.OemPlus,"Ctrl + Alt + OemPlus" },
                
            };
        }
    }
}