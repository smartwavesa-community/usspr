﻿using log4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Usspr.BrowserApp
{
    //code derivated from https://stackoverflow.com/questions/11285928/detecting-a-modal-dialog-box-of-another-process
    //and from https://gist.github.com/fjl/4080259
    public class AllWindowsHook
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(AllWindowsHook));
        private IntPtr myHook { set; get; }
        private IntPtr windowToAllow { set; get; }
        private bool isHookSet = false;

        public delegate void WinEventDelegate(IntPtr hWinEventHook, uint eventType,
    IntPtr hwnd, int idObject, int idChild, uint dwEventThread, uint dwmsEventTime);

        [DllImport("user32.dll")]
        public static extern IntPtr SetWinEventHook(uint eventMin, uint eventMax, IntPtr
           hmodWinEventProc, WinEventDelegate lpfnWinEventProc, uint idProcess,
           uint idThread, uint dwFlags);

        [DllImport("user32.dll")]
        public static extern bool UnhookWinEvent(IntPtr hWinEventHook);

        public void unhookWinEvent()
        {
            if (isHookSet)
                UnhookWinEvent(myHook);
        }

        // Constants from winuser.h
        public const uint EVENT_SYSTEM_FOREGROUND = 3;
        public const uint WINEVENT_OUTOFCONTEXT = 0;

        //The GetForegroundWindow function returns a handle to the foreground window.
        [DllImport("user32.dll")]
        public static extern IntPtr GetForegroundWindow();
        // For example, in Main() function
        // Listen for foreground window changes across all processes/threads on current desktop
        //public  IntPtr hhook = SetWinEventHook(EVENT_SYSTEM_FOREGROUND, EVENT_SYSTEM_FOREGROUND, IntPtr.Zero, new WinEventDelegate(WinEventProc), 0, 0, WINEVENT_OUTOFCONTEXT);
        public void SetWinEventHook(IntPtr allowThisWindow)
        {
            windowToAllow = allowThisWindow;
            isHookSet = true;
            myHook = SetWinEventHook(EVENT_SYSTEM_FOREGROUND, EVENT_SYSTEM_FOREGROUND, IntPtr.Zero, new WinEventDelegate(WinEventProc), 0, 0, WINEVENT_OUTOFCONTEXT);
            //EventLoop.Run();
        }

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        static extern int PostMessage(IntPtr hWnd, int msg, int wParam, int lParam);

        const int WM_CLOSE = 16;

        public void WinEventProc(IntPtr hWinEventHook, uint eventType,
        IntPtr hwnd, int idObject, int idChild, uint dwEventThread, uint dwmsEventTime)
        {
            IntPtr foregroundWinHandle = GetForegroundWindow();
            log.Debug("foregroundWinHandle: " + foregroundWinHandle.ToString());
            log.Debug("windowToAllow: " + windowToAllow.ToString());
            if (foregroundWinHandle != null && windowToAllow != null &&  foregroundWinHandle != windowToAllow)
            {
                log.Warn("WinEventProc: Blocking tentative to open a dialog");
                PostMessage(foregroundWinHandle, WM_CLOSE, 0, 0);
                log.Error("WinEventProc: Terminating process");
                Process.GetCurrentProcess().Kill();
            }
        }

        //When you Close Your application, remove the hook:
        //UnhookWinEvent(hhook);
    }
}
