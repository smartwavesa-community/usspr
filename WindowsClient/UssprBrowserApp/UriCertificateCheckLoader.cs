﻿using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Usspr.BrowserApp
{
    public class UriCertificateCheckLoader<T> : SsprUriLoader where T : SsprUriLoader, new()
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UriCertificateCheckLoader<T>));
        // Empty Certificate public key forces certificate pinning to be de-activated
        public static readonly String PUB_KEY = string.Empty;
        // public key for certificate pinning :
        // public static readonly String PUB_KEY = "3082010A0282010100F1CD8B80E58DD50B63EA51A7C9E7B8CD04FD1FEF8F7BDBCD19C10D3349B49200EBE139BB3ECABB9D24D280CC2B3E15275E12799FDD35BF6CEE04163355A7ED2D8B097274C09499A929A31D8AAC7B4DA15189D0D5F210934DC96013627E63400ADAB06AB06C398B2BA13F38781FCD6ED0A34E60027DDA9C4D085C68BEBB2DA07287B4087DD8EDEDE1C4D141CF95D892AFF1E98058AF04D423F6FFB142712A00981BEC375663D2A87FD0361626B85067804A164CB204A498595C35870BB802C15E0E678BBF247F63B0327450B36E64F3FF076D394021B914CD798EB4060356B6362BC47F07770F109F0F3124375CA4491578F414181242F58224A80704222C5DC10203010001";

        public UriCertificateCheckLoader() : this(true,false) 
        {
        }

        public UriCertificateCheckLoader(bool checkPubKey, bool includeParentCertificate) : base()
        {
            if (!String.IsNullOrEmpty(PUB_KEY))
            {
                log.Info("Certificate pinning activated");
                this.CheckPubKey = checkPubKey;
                this.IncludeParentCertificate = includeParentCertificate;
            }
            else
            {
                if (checkPubKey || includeParentCertificate)
                    log.Warn("Certificate KEY not defined. Certificate pinning de-activated");
                this.CheckPubKey = false;
                this.IncludeParentCertificate = false;
            }
            
        }



        protected override bool IsValid
        {
            get 
            {
                bool validates = false;
                if (this.SsprUri != null)
                {
                    validates = this.ValidatesServer();
                }
                if (!validates)
                    log.Error("Certificate or connection problem");
                return validates;
            }
        }


        private bool ValidatesServer()
        {
            log.Debug("ValidatesServer called");
            return TestConnection(this.SsprUri);
        }

        public bool TestConnection()
        {
            return IsValid;
        }

        private bool TestConnection(Uri uriToTest)
        {
            log.Debug("TestConnection called");
            try
            {
                log.Debug("Before test call to WebRequest.Create");
                // Request
                var request = (HttpWebRequest)WebRequest.Create(uriToTest);
                request.Timeout = 5000 /*milliseconds*/;
                AssignCertificateValidationCallback(request);

                // Response
                log.Debug("Before test call to request.GetResponse");
                using (var response = request.GetResponse())
                    log.Info("Test call done without error");
            }
            catch (WebException we)
            {
                log.Error(we.Message);
                return false;
            }
            catch (Exception e)
            {
                throw e;
            }
            return true;
        }

        public void AssignCertificateValidationCallback(HttpWebRequest request)
        {
            log.Debug("Assigning ServerCertificateValidationCallback to request");
            ServicePointManager.MaxServicePointIdleTime = 0;
            if (CheckPubKey && !IncludeParentCertificate)
                request.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(PinPublicKey);
            else if (CheckPubKey && IncludeParentCertificate)
                request.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(PinPublicKeyIncludeParent);
            else
                request.ServerCertificateValidationCallback = null;
        }

        public void AssignCertificateValidationCallback()
        {
            log.Debug("Assigning ServerCertificateValidationCallback to ServicePointManager");
            ServicePointManager.MaxServicePointIdleTime = 0;
            if (CheckPubKey && !IncludeParentCertificate)
                ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(PinPublicKey);
            else if (CheckPubKey && IncludeParentCertificate)
                ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(PinPublicKeyIncludeParent);
            else
                ServicePointManager.ServerCertificateValidationCallback = null;
        }

        // code from https://www.owasp.org/index.php/Certificate_and_Public_Key_Pinning#.Net
        public static bool PinPublicKey(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            //TODO: split Ssl errors check and public key check itself for proper reuse in PinPublicKeyIncludeParent
            log.Debug("PinPublicKey called");
#if DEBUG
            return true;
#endif
            if (certificate == null || chain == null)
            {
                log.Error("Certificate or chain is null");
                return false;
            }

            if (sslPolicyErrors != SslPolicyErrors.None)
            {
                if (sslPolicyErrors.HasFlag(SslPolicyErrors.RemoteCertificateNotAvailable))
                    log.Error("SSL Policy Error : RemoteCertificateNotAvailable");
                if (sslPolicyErrors.HasFlag(SslPolicyErrors.RemoteCertificateNameMismatch))
                    log.Error("SSL Policy Error : RemoteCertificateNameMismatch");
                if (sslPolicyErrors.HasFlag(SslPolicyErrors.RemoteCertificateChainErrors))
                    log.Error("SSL Policy Error : RemoteCertificateChainErrors");
                return false;
            }


            // Verify against known public key within the certificate
            log.Debug("Checking pinned public key");
            String pk = certificate.GetPublicKeyString();
            log.Debug("Server certificate public key : " + pk);
            log.Debug("Pinned certificate public key : " + PUB_KEY);

            if (pk.ToUpperInvariant().Equals(PUB_KEY.ToUpperInvariant()))
            {
                log.Info("Certificate public key is the same as the pinned public key.");
                return true;
            }

            log.Error("Certificate public key is NOT the same as the pinned public key.");
            return false;
        }


        

        public static bool PinPublicKeyIncludeParent(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            throw new NotImplementedException();
            log.Debug("PinPublicKeyIncludeParent called");
#if DEBUG
            return true;
#endif
            if (PinPublicKey(sender, certificate, chain, sslPolicyErrors))
            {
                return true;
            }          
            else
            {
                String pk = certificate.GetPublicKeyString();
                log.Info("Checking chain of certificates for the pinned public key");
                foreach (var c in chain.ChainElements)
                {
                    if (c.Certificate.GetPublicKeyString().ToUpperInvariant().Equals(PUB_KEY.ToUpperInvariant()))
                    {
                        log.Info("Found the pinned public key in certificate chain");
                        return true;
                    }   
                }
            }
            log.Error("The pinned public key was NOT found in certificate chain");
            return false;
        }


        protected override void Load()
        {
            this.SsprUri = new T().SsprUri;
        }

        private bool IncludeParentCertificate { get; set; }

        private bool CheckPubKey { get; set; }
    }
}
