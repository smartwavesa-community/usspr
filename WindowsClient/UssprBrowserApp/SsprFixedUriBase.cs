﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Usspr.BrowserApp
{
    public abstract class SsprFixedUriBase : SsprUriLoader
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(SsprFixedUriBase));
        private string candidateSspruri = String.Empty;

        public SsprFixedUriBase() : base() { }

        public SsprFixedUriBase(string url) : this()
        {
            this.candidateSspruri = url;
        }
        protected override void Load()
        {
            try
            {
                log.Debug("Checking string format while trying to load");
                if (string.IsNullOrWhiteSpace(this.candidateSspruri))
                    throw new ArgumentNullException();
                this.SsprUri = new Uri(this.candidateSspruri);
            }
            catch (UriFormatException ufe)
            {
                log.Error("Not a valid Uri: " + ufe.Message);
                this.SsprUri = null;
            }
            catch (ArgumentNullException ane)
            {
                log.Error("Argument is null or empty : " + ane.Message);
                this.SsprUri = null;
            }
        }

        protected override bool IsValid
        {
            get
            {
                bool validates = false;
                if (this.SsprUri != null)
                {
                    validates = this.SsprUri.Scheme == Uri.UriSchemeHttps;
                }
                if (!validates)
                    log.Error("Invalid Usspr Uri");
                return validates;
            }
        }
    }
}
