﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;
using System.Security;
using System.IO;
using log4net;
using Usspr.BrowserApp.Properties;
using System.Security.Cryptography;

namespace Usspr.BrowserApp
{
    public class SsprUriRegistryLoader : SsprUriLoader
    {
        const string REG_PATH = @"HKEY_LOCAL_MACHINE\SOFTWARE\Usspr";
        const string REG_ENTRY_NAME = @"SsprUrl";
        private static readonly ILog log = LogManager.GetLogger(typeof(SsprUriRegistryLoader));
        private const string DEFAULT_SALT = "{3D697BCD-5356-4B25-A6B7-B4F1FBD96AD8}";

        public SsprUriRegistryLoader() : base() { }

        private string getUrlFromRegistry()
        {
            log.Debug("Trying to get Usspr Url from Registry");
            try
            {
                return (string)Registry.GetValue(REG_PATH, REG_ENTRY_NAME, null);
            }
            catch (SecurityException se)
            {
                log.Error("Security Exception while trying to load: " + se.Message);
                return null;
            }
            catch (IOException ioe)
            {
                log.Error("IOException Exception while trying to load: " + ioe.Message);
                return null;
            }
            catch (ArgumentException ae)
            {
                log.Error("ArgumentException Exception while trying to load: " + ae.Message);
                return null;
            }
        }

        protected override void Load()
        {
            try
            {
                log.Debug("Checking string format while trying to load");
                this.SsprUri = new Uri(this.getUrlFromRegistry());
            }
            catch (UriFormatException ufe)
            {
                log.Error("Not a valid Uri: " + ufe.Message);
                this.SsprUri = null;
            }
            catch (ArgumentNullException ane)
            {
                log.Error("Argument is null: " + ane.Message);
                this.SsprUri = null;
            }
        }
            

        protected override bool IsValid
        {
            get
            {
                bool validates = false;
                if (this.SsprUri != null)
                {
                    validates = this.SsprUri.Scheme == Uri.UriSchemeHttps && this.HasValidChecksum();
                }
                if (!validates)
                    log.Error("Invalid Usspr Uri");
                return validates;
            }
        }

        private bool HasValidChecksum()
        {
            var chkFromConfig = Settings.Default.UssprUriChecksum;
            var ckValid = (null != this.SsprUri && null != chkFromConfig && chkFromConfig == Hash(this.SsprUri.ToString()));
            if (!ckValid)
                log.Error("Invalid checksum");
            return ckValid;
        }

        public static string Hash(string input, string input2)
        {
            var hash = (new SHA256Managed()).ComputeHash(Encoding.UTF8.GetBytes(input + GetSalt(input2)));
            return string.Join("", hash.Select(b => b.ToString("x2")).ToArray());
        }

        private string Hash(string input)
        {
            return Hash(input, GetPathFromRegistry());
        }

        private string GetPathFromRegistry()
        {
            string imagePathRegEntry = @"CredentialProviderTileImagePath";
            log.Debug("Trying to get Usspr Image path from Registry");
            try
            {
                return (string)Registry.GetValue(REG_PATH, imagePathRegEntry, null);
            }
            catch (SecurityException se)
            {
                log.Error("Security Exception while trying to load image path: " + se.Message);
                return null;
            }
            catch (IOException ioe)
            {
                log.Error("IOException Exception while trying to load image path: " + ioe.Message);
                return null;
            }
            catch (ArgumentException ae)
            {
                log.Error("ArgumentException Exception while trying to load image path: " + ae.Message);
                return null;
            }
        }

        private static string GetSalt(string fileLocation)
        {
            if (String.IsNullOrWhiteSpace(fileLocation) || !(File.Exists(fileLocation)))
                return DEFAULT_SALT;
            using (FileStream fs = File.OpenRead(fileLocation))
            {
                var hash = (new SHA1Managed()).ComputeHash(fs);
                return string.Join("", hash.Select(b => b.ToString("x2")).ToArray());
            }
        }
    }
}
