﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Navigation;
using Usspr.BrowserApp.Helpers;
using mshtml;
using System.Diagnostics;
using System.Reflection;
using System.Windows.Threading;
using SHDocVw;
using System.Windows.Interop;
using Usspr.BrowserApp.Properties;
using log4net;
using log4net.Config;
using Microsoft.Win32;
using System.Net;
using System.Runtime.InteropServices;

namespace Usspr.BrowserApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(MainWindow));
        private static InstanceManager instance = InstanceManager.Instance;
        private bool ieContextMenuAlreadyDisabledBefore = false;
        private UriCertificateCheckLoader<SsprUriRegistryLoader> certificateCheckLoader { get; set; }
        private Uri browseTo { get; set; }

        private AllWindowsHook awh = new AllWindowsHook();
        
        public MainWindow()
        {
            XmlConfigurator.Configure();
            ConditionallyStart(false);
        }

        private void ConditionallyStart(bool startWithSystemIntegrityLevel)
        {
            if (startWithSystemIntegrityLevel)
                StartWithSystemIntegrity();
            else
                StartWithLowerIntegrity();
        }

        private void StartWithLowerIntegrity()
        {
            log.Debug("Called StartWithLowerIntegrity()");
            var process = Process.GetCurrentProcess();
            string executablePath = process.MainModule.FileName;
            Process[] processes = Process.GetProcessesByName(process.ProcessName);
            int IntegrityLevel = instance.GetProcessIntegrityLevel();
            log.Info("Your Integrity Level is :" + IntegrityLevel);

            if (IntegrityLevel <= NativeMethod.SECURITY_MANDATORY_MEDIUM_RID)
            {
                log.Info("Starting BrowserApp");
                startingBrowserApp();
            }
            else if (processes.Length == 1 && (IntegrityLevel >= NativeMethod.SECURITY_MANDATORY_HIGH_RID))
            {
                log.Info("Starting Creating medium integrity process");
                instance.CreateMediumIntegrityProcess(executablePath);
                terminateCurrentProcess();
            }
            else
            {
                log.Info("Terminating process");
                terminateCurrentProcess();
            }
        }

        private void StartWithSystemIntegrity()
        {
            log.Debug("Called StartWithSystemIntegrity()");
            var process = Process.GetCurrentProcess();
            Process[] processes = Process.GetProcessesByName(process.ProcessName);
            int IntegrityLevel = instance.GetProcessIntegrityLevel();
            log.Info("Your Integrity Level is :" + IntegrityLevel);

            //if an instance is already running and the current process integrity level is not SYSTEM we block it
            if (processes.Length == 1 && (IntegrityLevel.Equals(NativeMethod.SECURITY_MANDATORY_SYSTEM_RID)))
            {
                log.Info("No other process is running, starting...");
                startingBrowserApp();
            }
            else
            {
                log.Info("Not the right integrity level or more than one process running... shutting down");
                terminateCurrentProcess();
            }
        }

        private void startingBrowserApp()
        {
            log.Info("Starting Usspr Browser application");
            
            ieContextMenuAlreadyDisabledBefore = this.DisableRightClickForAll(1);
            this.Closing += MainWindow_Closing;

            this.IsEnabled = false;
            Dispatcher.BeginInvoke(DispatcherPriority.Loaded, new Action(() =>
            {
                this.DisableContextualMenuAndScriptErrors();
                this.HandleNativeBrowserEvents();
                awh.SetWinEventHook((new WindowInteropHelper(this)).Handle);
            }));
            
            InitializeComponent();

            log.Info("Initialized component");
            int timeout = Settings.Default.InactiveTimeoutSeconds;
            if (timeout != 0)
            {
                DefineInactivityTimer(timeout);
            }
            else
            {
                log.Debug("Inactivity Timeout de-activated");
            }

            this.MaxHeight = SystemParameters.MaximizedPrimaryScreenHeight;// -SystemParameters.MenuBarHeight;
            this.MinHeight = SystemParameters.MaximizedPrimaryScreenHeight;// -SystemParameters.MenuBarHeight;
            this.MaxWidth = SystemParameters.MaximizedPrimaryScreenWidth;// -SystemParameters.BorderWidth * 2;
            this.MinWidth = SystemParameters.MaximizedPrimaryScreenWidth;// -SystemParameters.BorderWidth * 2;
            
            var ksbh = new KeyboardShortcutsBlockerHelper(this);
            ksbh.AddHandlers();
            var mebh = new MouseEventsBlockerHelper(this);
            mebh.AddHandlers();
            
            log.Debug("Blocking the faculty to move the window from the taskbar and to open dialogs");
            this.SourceInitialized += Window1_SourceInitialized;
            log.Debug("Blocked the faculty to move the window from the taskbar and to open dialogs");

            this.SetCustomWindowsTitle();

            log.Debug("Starting Usspr Browser application: init complete. Trying to load the Usspr front-end from the server.");
            certificateCheckLoader = new UriCertificateCheckLoader<SsprUriRegistryLoader>();
            browseTo = certificateCheckLoader.SsprUri;

            this.IsEnabled = true;
            if (browseTo != null)
            {
                log.Info("Successfully loaded Usspr front-end.");
                ssprBrowser.Navigating += ssprBrowser_Navigating;
                ssprBrowser.Navigated += SsprBrowser_Navigated;
                ssprBrowser.LoadCompleted += SsprBrowser_LoadCompleted;
                ssprBrowser.Navigate(browseTo);
            }
            else
            {
                log.Error("Failed loading the Usspr front-end.");
                this.Close();
            }
        }

        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //re-enable unless it was disabled before
            if (!ieContextMenuAlreadyDisabledBefore)
                this.DisableRightClickForAll(0);

            awh.unhookWinEvent();
        }

        /// <summary>
        /// Disable (or re-enable) context menu for all internet explorer instance and all users 
        /// </summary>
        /// <param name="regDataValue"></param>
        /// <returns>True if the key was present and already disabled</returns>
        private bool DisableRightClickForAll(int regDataValue)
        {
            //const string REG_PATH = @"HKEY_LOCAL_MACHINE\SOFTWARE\UssprHKLM\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer";
            //const string REG_ENTRY_NAME = @"NoViewContextMenu";
            const string REG_PATH = @"HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Internet Explorer\Restrictions";
            const string REG_ENTRY_NAME = @"NoBrowserContextMenu";
            bool isAlreadyDisabled = ((Registry.GetValue(REG_PATH, REG_ENTRY_NAME,0) as int? ??0) == 1);
            Registry.SetValue(REG_PATH, REG_ENTRY_NAME, regDataValue, RegistryValueKind.DWord);
            return isAlreadyDisabled;
        }

        private void terminateCurrentProcess()
        {
            Process.GetCurrentProcess().Kill();
        }

        private void HandleNativeBrowserEvents()
        {
            log.Debug("Handling native browser events");
            IWebBrowser2 nativeBrowser = typeof(System.Windows.Controls.WebBrowser).GetProperty("AxIWebBrowser2", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(this.ssprBrowser, null) as IWebBrowser2; //as SHDocVw.IWebBrowser2;
            SHDocVw.DWebBrowserEvents_Event wbEvents = (SHDocVw.DWebBrowserEvents_Event)nativeBrowser;

            wbEvents.NewWindow += new SHDocVw.DWebBrowserEvents_NewWindowEventHandler(OnWebBrowserNewWindow);
            log.Debug("Disabled new window events");
        }

        private void DisableContextualMenuAndScriptErrors()
        {
            log.Debug("Disabling contextual menu and script errors");
            WebBrowserHostUIHandler wbHandler = new WebBrowserHostUIHandler(ssprBrowser);
            wbHandler.IsWebBrowserContextMenuEnabled = false;
            wbHandler.ScriptErrorsSuppressed = true;
            log.Debug("Disabled contextual menu and script errors");
        }

        /// <summary>
        /// The application should close if no navigation occured after the navigation timeout
        /// </summary>
        private void DefineInactivityTimer(int timeoutInSeconds)
        {
            log.Debug(string.Format("Starting inactivity timer with a timeout of {0} seconds", timeoutInSeconds));
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Start();
            int tickCounter = timeoutInSeconds;

            var titleExplanation = new Loaders.TitleExplanation();
            titleExplanation.BuildFromRegistry();

            this.ssprBrowser.Navigating += delegate
            {
                log.Debug(string.Format("Navigating event: resetting inactivity timer with a timeout of {0} seconds", timeoutInSeconds));
                tickCounter = timeoutInSeconds;
                this.Title = titleExplanation.ToString(timeoutInSeconds.ToString());
                timer.Stop();
                timer.Start();
            };
            timer.Tick += delegate
            {
                tickCounter--;
                this.Title = titleExplanation.ToString(tickCounter.ToString());
                if (tickCounter == 0)
                {
                    timer.Stop();
                    this.Close();
                }
            };
        }

        private void SetCustomWindowsTitle()
        {
            var titleExplanation = new Loaders.TitleExplanation();
            titleExplanation.BuildFromRegistry();
            this.Title = titleExplanation.ToString("");
        }

        const int WM_SYSCOMMAND = 0x0112;
        const int SC_MOVE = 0xF010;
        private const int WM_INITDIALOG = 0x0110;
        private const int WM_NCLBUTTONDBLCLK = 0x00A3;
        private const int SC_RESTORE = 0xF120;
        /// <summary>
        /// Block the faculty to move the window from the taskbar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window1_SourceInitialized(object sender, EventArgs e)
        {
            WindowInteropHelper helper = new WindowInteropHelper(this);
            HwndSource source = HwndSource.FromHwnd(helper.Handle);
            source.AddHook(WndProc);
        }

        private IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            //log.Debug("entered WndProc with msg :" + msg);
            switch (msg)
            {
                case WM_SYSCOMMAND:
                    int command = wParam.ToInt32() & 0xfff0;
                    if (command == SC_MOVE)
                    {
                        handled = true;
                    }
                    if (command == SC_RESTORE)
                    {
                        handled = true;
                    }

                    break;
                case WM_NCLBUTTONDBLCLK:
                    if (this.WindowState != System.Windows.WindowState.Maximized)
                        this.WindowState = System.Windows.WindowState.Maximized;
                    break;
                case WM_INITDIALOG:
                    {
                        log.Info("blocked dialog");
                        handled = true;
                        this.Close();
                        return new IntPtr(1);
                    }
                default:
                    break;
            }
            return IntPtr.Zero;
        }

        private void OnWebBrowserNewWindow(string URL, int Flags, string TargetFrameName, ref object PostData, string Headers, ref bool Processed)
        {
            Processed = true;
            ssprBrowser.Navigate(URL);
        }

        /// <summary>
        /// Force all Links to open in the same window (html target property)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ssprBrowser_LoadCompleted(object sender, NavigationEventArgs e)
        {
            log.Debug("entering ssprBrowser_LoadCompleted (original)");
            AddHstsHeaderIfMissing(e.WebResponse);
            HTMLDocument doc2 = ssprBrowser.Document as HTMLDocument;

            foreach (IHTMLElement item in doc2.links)
            {
                var targetAttributeValue = item.getAttribute("target");
                if (targetAttributeValue != null)
                {
                    //Debug.WriteLine(item.getAttribute("href") as string + " changed href from " + targetAttributeValue as string + " to _self");
                    item.setAttribute("target", "_self");
                }
            }

            //Block context menu
            var doc = (HTMLDocumentEvents2_Event)this.ssprBrowser.Document;
            doc.oncontextmenu += obj => false;

        }

        private void ssprBrowser_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                log.Info("Escape key pressed. Closing...");
                this.Close();
            }
        }


        void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void ssprBrowser_Navigating(object sender, NavigatingCancelEventArgs e)
        {
            // Make sure the hostname has not changed.
            log.Info("Navigating to " + e.Uri.ToString());
            if (HostnameHasChanged(e))
            {
                log.Error("Host name has changed. Cancelling navigation and terminating process.");
                e.Cancel = true;
                terminateCurrentProcess();
            }

            //TODO: make this version work
            //certificateCheckLoader.AssignCertificateValidationCallback((HttpWebRequest)e.WebRequest);

            if (!certificateCheckLoader.TestConnection())
            {
                log.Error("Connection test failed. Cancelling navigation and terminating process.");
                e.Cancel = true;
                terminateCurrentProcess();
            }
        }

        private bool HostnameHasChanged(NavigatingCancelEventArgs e)
        {
            return String.IsNullOrWhiteSpace(e.Uri.ToString())
                            || String.IsNullOrWhiteSpace((browseTo?.Host) ?? String.Empty)
                            || !e.Uri.Host.Equals(browseTo.Host);
        }

        private void SsprBrowser_LoadCompleted(object sender, NavigationEventArgs e)
        {
            log.Debug("Entering SsprBrowser_LoadCompleted... New");
            //e.WebResponse is always null
            //AddHstsHeaderIfMissing(e.WebResponse);
        }

        private void SsprBrowser_Navigated(object sender, NavigationEventArgs e)
        {
            log.Debug("Entering SsprBrowser_Navigated");
            //e.WebResponse is always null
            //AddHstsHeaderIfMissing(e.WebResponse);
        }

        private void MainWindow_Navigated(object sender, NavigationEventArgs e)
        {
            log.Debug("Entering MainWindow_Navigated");
            //e.WebResponse is always null
            //AddHstsHeaderIfMissing(e.WebResponse);
        }

        private void AddHstsHeaderIfMissing(WebResponse r)
        {
            if (!ResponseContainsHstsHeader(r))
            {
                log.Info("Adding missing HSTS header");
                (r as HttpWebResponse).Headers.Add("Strict-Transport-Security", "max-age=31536000;includeSubdomains");
                if (!ResponseContainsHstsHeader(r))
                {
                    log.Warn("Could not add HSTS header");
                }
            }
            else
            {
                log.Info("HSTS header is already present");
            }
        }

        private bool ResponseContainsHstsHeader(WebResponse r)
        {
            try
            {
                if (r == null)
                {
                    log.Error("WebResponse is null");
                    return false;
                }
                if (!(r is HttpWebResponse))
                {
                    log.Warn("r is not an HttpWebResponse, but a " + r.GetType().ToString());
                    return false;
                }
                var result = (null != (r as HttpWebResponse).GetResponseHeader("Strict-Transport-Security"));
                log.Debug("ResponseContainsHstsHeader result: " + result);
                return result;
            }
            catch (Exception e)
            {
                log.Error("Error when testing ResponseContainsHstsHeader", e);
                return false;
            }
        }
    }
}