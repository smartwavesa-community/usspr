﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Usspr.BrowserApp
{
    /// <summary>
    /// This class is used to provide the web browser application with a fixed value for the Usspr server Url.
    /// TODO: change this class to work with a build parameter (for example, with templating)
    /// </summary>
    public class MyFixedUri : SsprFixedUriBase
    {
        public MyFixedUri() : base(@"https://localhost") { }
    }
}
