﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Usspr.BrowserApp
{
    public abstract class SsprUriLoader
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(SsprUriLoader));
        public Uri SsprUri
        { get; protected set; }

        public SsprUriLoader()
        {
            log.Debug("Loading Usspr Uri");
            this.Load();
            if (!this.IsValid)
            {
                log.Warn("Invalid Usspr Uri found. Returning null.");
                this.SsprUri = null;
            }
            else
            {
                log.Debug("Successfully loaded Usspr Uri");
            }
        }

        protected abstract bool IsValid { get; }

        protected abstract void Load();
    }
}
