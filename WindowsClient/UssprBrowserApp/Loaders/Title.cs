﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Usspr.BrowserApp.Loaders
{
    public class Title : AbstractTitle
    {
        public Title() : base("Forgot Password")
        {
        }
        public override void BuildFromRegistry()
        {
            BuildFromRegistry(@"CredentialProviderTileLabel");
        }

        public override void TryParseOrDefault(string toParse)
        {
            TryParseOrDefault(toParse, 32, @"^[a-zA-Zéàöäèüöê.:!? ']+$");
        }
    }
}
