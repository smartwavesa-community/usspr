﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Usspr.BrowserApp.Loaders
{
    public class TitleExplanation : AbstractTitle
    {
        private static readonly string TIMER_PLACEHOLDER = "!TIMELEFT!";
        public TitleExplanation() : base($"Time left: {TIMER_PLACEHOLDER}s")
        {
        }
        public override void BuildFromRegistry()
        {
            BuildFromRegistry(@"CredentialProviderTileLabelExplanation");
        }

        public override void TryParseOrDefault(string toParse)
        {
            TryParseOrDefault(toParse, 120, @"^[a-zA-Z0-9éàöäèüöê.:!? ']+$");
        }

        /// <summary>
        /// Get value while replacing Timer Placeholder by requested parameter
        /// </summary>
        /// <param name="timerValue"></param>
        /// <returns></returns>
        public string ToString(string timerValue)
        {
            return this.Value.Replace(TIMER_PLACEHOLDER, timerValue);
        }
    }
}
