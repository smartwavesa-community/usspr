﻿using log4net;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Usspr.BrowserApp.Loaders
{
    public abstract class AbstractTitle
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(AbstractTitle));
        public string DefaultValue { private set; get; }
        public string Value { private set; get; }

        protected AbstractTitle(string defaultValue)
        {
            this.DefaultValue = defaultValue;
            this.Value = DefaultValue;
        }

        public override string ToString()
        {
            return Value;
        }

        protected void TryParseOrDefault(string toParse,int maxLength, string allowedCharacters)
        {
            this.Value = (string.IsNullOrWhiteSpace(toParse) 
                || toParse.Length > maxLength 
                || (!Regex.IsMatch(toParse, allowedCharacters, RegexOptions.IgnoreCase)) 
                ? this.DefaultValue 
                : toParse);
        }

        public abstract void TryParseOrDefault(string toParse);

        protected void BuildFromRegistry(string regEntryName)
        {
            string titleFromRegistry = string.Empty;
            log.Debug($"Trying to load {regEntryName} from registry");
            string regPath = @"HKEY_LOCAL_MACHINE\SOFTWARE\Usspr";
            titleFromRegistry = (string)Registry.GetValue(regPath, regEntryName, null);
            log.Debug($"Loaded {regEntryName} from registry: {titleFromRegistry}");
            this.TryParseOrDefault(titleFromRegistry);
        }

        public abstract void BuildFromRegistry();
    }
}
