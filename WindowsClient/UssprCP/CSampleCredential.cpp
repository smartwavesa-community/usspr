//
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
//
// Copyright (c) Microsoft Corporation. All rights reserved.
//
//

#ifndef WIN32_NO_STATUS
#include <ntstatus.h>
#define WIN32_NO_STATUS
#endif
#include <unknwn.h>
#include "CSampleCredential.h"
#include "guid.h"
#include <stdio.h> 
#include <iostream>
#include <windows.h>
#include <windowsx.h>
#include <shlobj.h>
#include <sstream>
#include <string>
#include <fstream>
#include "sha256.h" //library downloaded from http://create.stephan-brumme.com/hash-library/
#include "browserapphash.h"
#include "detectfx_new.h"



// CSampleCredential ////////////////////////////////////////////////////////

CSampleCredential::CSampleCredential() :
	_cRef(1),
	_pCredProvCredentialEvents(nullptr)
{
	DllAddRef();

	ZeroMemory(_rgCredProvFieldDescriptors, sizeof(_rgCredProvFieldDescriptors));
	ZeroMemory(_rgFieldStatePairs, sizeof(_rgFieldStatePairs));
	ZeroMemory(_rgFieldStrings, sizeof(_rgFieldStrings));
}

CSampleCredential::~CSampleCredential()
{
	for (int i = 0; i < ARRAYSIZE(_rgFieldStrings); i++)
	{
		CoTaskMemFree(_rgFieldStrings[i]);
		CoTaskMemFree(_rgCredProvFieldDescriptors[i].pszLabel);
	}

	DllRelease();
}

// Initializes one credential with the field information passed in.
// Set the value of the SFI_USERNAME field to pwzUsername.
// Optionally takes a password for the SetSerialization case.
HRESULT CSampleCredential::Initialize(
	__in const CREDENTIAL_PROVIDER_FIELD_DESCRIPTOR* rgcpfd,
	__in const FIELD_STATE_PAIR* rgfsp,
	__in PCWSTR pwzUsername,
	__in PCWSTR pwzPassword
)
{
	UNREFERENCED_PARAMETER(pwzPassword);
	HRESULT hr = S_OK;

	// Copy the field descriptors for each field. This is useful if you want to vary the 
	// field descriptors based on what Usage scenario the credential was created for.
	for (DWORD i = 0; SUCCEEDED(hr) && i < ARRAYSIZE(_rgCredProvFieldDescriptors); i++)
	{
		_rgFieldStatePairs[i] = rgfsp[i];
		hr = FieldDescriptorCopy(rgcpfd[i], &_rgCredProvFieldDescriptors[i]);
	}

	// Initialize the String values of all the fields.
	if (SUCCEEDED(hr))
	{
		hr = SHStrDupW(pwzUsername, &_rgFieldStrings[SFI_USERNAME]);
	}
	if (SUCCEEDED(hr))
	{
		hr = SHStrDupW(L"Reset my password", &_rgFieldStrings[SFI_SUBMIT_BUTTON]);
	}

	return S_OK;
}

// LogonUI calls this in order to give us a callback in case we need to notify it of anything.
HRESULT CSampleCredential::Advise(
	__in ICredentialProviderCredentialEvents* pcpce
)
{
	if (_pCredProvCredentialEvents != nullptr)
	{
		_pCredProvCredentialEvents->Release();
	}
	return pcpce->QueryInterface(IID_PPV_ARGS(&_pCredProvCredentialEvents));
}

// LogonUI calls this to tell us to release the callback.
HRESULT CSampleCredential::UnAdvise()
{
	if (_pCredProvCredentialEvents)
	{
		_pCredProvCredentialEvents->Release();
	}
	_pCredProvCredentialEvents = nullptr;
	return S_OK;
}

// LogonUI calls this function when our tile is selected (zoomed).
// If you simply want fields to show/hide based on the selected state,
// there's no need to do anything here - you can set that up in the 
// field definitions.  But if you want to do something
// more complicated, like change the contents of a field when the tile is
// selected, you would do it here.
HRESULT CSampleCredential::SetSelected(__out BOOL* pbAutoLogon)
{
	//directly press the submit button when the tile is selected
	*pbAutoLogon = TRUE;
	return S_OK;
}

// Similarly to SetSelected, LogonUI calls this when your tile was selected
// and now no longer is. The most common thing to do here (which we do below)
// is to clear out the password field.
HRESULT CSampleCredential::SetDeselected()
{
	return S_OK;
}

// Gets info for a particular field of a tile. Called by logonUI to get information to 
// display the tile.
HRESULT CSampleCredential::GetFieldState(
	__in DWORD dwFieldID,
	__out CREDENTIAL_PROVIDER_FIELD_STATE* pcpfs,
	__out CREDENTIAL_PROVIDER_FIELD_INTERACTIVE_STATE* pcpfis
)
{
	HRESULT hr;

	// Validate paramters.
	if ((dwFieldID < ARRAYSIZE(_rgFieldStatePairs)) && pcpfs && pcpfis)
	{
		*pcpfs = _rgFieldStatePairs[dwFieldID].cpfs;
		*pcpfis = _rgFieldStatePairs[dwFieldID].cpfis;

		hr = S_OK;
	}
	else
	{
		hr = E_INVALIDARG;
	}
	return hr;
}

// Sets ppwsz to the string value of the field at the index dwFieldID.
HRESULT CSampleCredential::GetStringValue(
	__in DWORD dwFieldID,
	__deref_out PWSTR* ppwsz
)
{
	HRESULT hr;

	// Check to make sure dwFieldID is a legitimate index.
	if (dwFieldID < ARRAYSIZE(_rgCredProvFieldDescriptors) && ppwsz)
	{
		// Make a copy of the string and return that. The caller
		// is responsible for freeing it.
		hr = SHStrDupW(_rgFieldStrings[dwFieldID], ppwsz);
	}
	else
	{
		hr = E_INVALIDARG;
	}

	return hr;
}

LONG GetDWORDRegKey(HKEY hKey, const std::wstring &strValueName, DWORD &nValue, DWORD nDefaultValue)
{
	nValue = nDefaultValue;
	DWORD dwBufferSize(sizeof(DWORD));
	DWORD nResult(0);
	LONG nError = ::RegQueryValueExW(hKey,
		strValueName.c_str(),
		0,
		NULL,
		reinterpret_cast<LPBYTE>(&nResult),
		&dwBufferSize);
	if (ERROR_SUCCESS == nError)
	{
		nValue = nResult;
	}
	return nError;
}


LONG GetBoolRegKey(HKEY hKey, const std::wstring &strValueName, bool &bValue, bool bDefaultValue)
{
	DWORD nDefValue((bDefaultValue) ? 1 : 0);
	DWORD nResult(nDefValue);
	LONG nError = GetDWORDRegKey(hKey, strValueName.c_str(), nResult, nDefValue);
	if (ERROR_SUCCESS == nError)
	{
		bValue = (nResult != 0) ? true : false;
	}
	return nError;
}


LONG GetStringRegKey(HKEY hKey, const std::wstring &strValueName, std::wstring &strValue, const std::wstring &strDefaultValue)
{
	strValue = strDefaultValue;
	WCHAR szBuffer[512];
	DWORD dwBufferSize = sizeof(szBuffer);
	ULONG nError;
	nError = RegQueryValueExW(hKey, strValueName.c_str(), 0, NULL, (LPBYTE)szBuffer, &dwBufferSize);
	if (ERROR_SUCCESS == nError)
	{
		strValue = szBuffer;
	}
	return nError;
}


// The goal is to check to size and nature of the bmp file in parameter.
// Code inspired by the example on http://tipsandtricks.runicsoft.com/Cpp/BitmapTutorial.html
bool CheckIsAllowedBMP(LPCTSTR bmpfile)
{
	BITMAPFILEHEADER bmpheader;
	BITMAPINFOHEADER bmpinfo;
	DWORD bytesread;
	HANDLE file = CreateFile(bmpfile, GENERIC_READ, FILE_SHARE_READ,
		NULL, OPEN_EXISTING, FILE_FLAG_SEQUENTIAL_SCAN, NULL);
	if (NULL == file)
		return false;

	if (ReadFile(file, &bmpheader, sizeof(BITMAPFILEHEADER),
		&bytesread, NULL) == false)
	{
		CloseHandle(file);
		return false;
	}

	if (ReadFile(file, &bmpinfo, sizeof(BITMAPINFOHEADER),
		&bytesread, NULL) == false)
	{
		CloseHandle(file);
		return false;
	}

	if (bmpheader.bfType != 'MB')
	{
		CloseHandle(file);
		return false;
	}

	if (bmpinfo.biCompression != BI_RGB)
	{
		CloseHandle(file);
		return false;
	}
	if (bmpinfo.biBitCount != 24)
	{
		CloseHandle(file);
		return false;
	}

	if (!((bmpinfo.biWidth == 192 && abs(bmpinfo.biHeight) == 192) || (bmpinfo.biWidth == 128 && abs(bmpinfo.biHeight) == 128)))
		return false;

	CloseHandle(file);
	return true;
}



// Gets the image to show in the user tile.
HRESULT CSampleCredential::GetBitmapValue(
	__in DWORD dwFieldID,
	__out HBITMAP* phbmp
)
{
	HRESULT hr;
	if ((SFI_TILEIMAGE == dwFieldID) && phbmp)
	{
		//Check if the optional registry setting CredentialProviderTileImagePath is defined. If it is, try to load external image.
		HKEY hKey = NULL;
		LONG lRes = RegOpenKeyExW(HKEY_LOCAL_MACHINE, L"SOFTWARE\\Usspr", 0, KEY_READ, &hKey);
		UNREFERENCED_PARAMETER(lRes);
		std::wstring strValueOfBinDir;
		GetStringRegKey(hKey, L"CredentialProviderTileImagePath", strValueOfBinDir, L"bad");

		HBITMAP hbmp = NULL;
		if (strValueOfBinDir != L"bad" && PathFileExists(strValueOfBinDir.c_str()) && CheckIsAllowedBMP(strValueOfBinDir.c_str()))
		{
			hbmp = (HBITMAP)LoadImage(NULL, strValueOfBinDir.c_str(), IMAGE_BITMAP, 192, 192, LR_DEFAULTCOLOR | LR_LOADFROMFILE);

			if (NULL == hbmp) {
				hbmp = (HBITMAP)LoadImage(NULL, strValueOfBinDir.c_str(), IMAGE_BITMAP, 128, 128, LR_DEFAULTCOLOR | LR_LOADFROMFILE);
			}
		}
		else
		{
			//load default image
			hbmp = LoadBitmap(HINST_THISDLL, MAKEINTRESOURCE(IDB_TILE_IMAGE));
		}

		if (hbmp != NULL)
		{
			hr = S_OK;
			*phbmp = hbmp;
		}
		else
		{
			hr = HRESULT_FROM_WIN32(GetLastError());
		}
	}
	else
	{
		hr = E_INVALIDARG;
	}

	return hr;
}

// Sets pdwAdjacentTo to the index of the field the submit button should be 
// adjacent to. We recommend that the submit button is placed next to the last
// field which the user is required to enter information in. Optional fields
// should be below the submit button.
HRESULT CSampleCredential::GetSubmitButtonValue(
	__in DWORD dwFieldID,
	__out DWORD* pdwAdjacentTo
)
{
	HRESULT hr;

	// Validate parameters.
	if ((SFI_SUBMIT_BUTTON == dwFieldID) && pdwAdjacentTo)
	{
		// pdwAdjacentTo is a pointer to the fieldID you want the submit button to appear next to.
		*pdwAdjacentTo = SFI_USERNAME;
		hr = S_OK;
	}
	else
	{
		hr = E_INVALIDARG;
	}
	return hr;
}

// Sets the value of a field which can accept a string as a value.
// This is called on each keystroke when a user types into an edit field.
HRESULT CSampleCredential::SetStringValue(
	__in DWORD dwFieldID,
	__in PCWSTR pwz
)
{
	HRESULT hr;

	// Validate parameters.
	if (dwFieldID < ARRAYSIZE(_rgCredProvFieldDescriptors) &&
		(CPFT_EDIT_TEXT == _rgCredProvFieldDescriptors[dwFieldID].cpft ||
			CPFT_PASSWORD_TEXT == _rgCredProvFieldDescriptors[dwFieldID].cpft))
	{
		PWSTR* ppwszStored = &_rgFieldStrings[dwFieldID];
		CoTaskMemFree(*ppwszStored);
		hr = SHStrDupW(pwz, ppwszStored);
	}
	else
	{
		hr = E_INVALIDARG;
	}

	return hr;
}

//------------- 
// The following methods are for logonUI to get the values of various UI elements and then communicate
// to the credential about what the user did in that field.  However, these methods are not implemented
// because our tile doesn't contain these types of UI elements
HRESULT CSampleCredential::GetCheckboxValue(
	__in DWORD dwFieldID,
	__out BOOL* pbChecked,
	__deref_out PWSTR* ppwszLabel
)
{
	UNREFERENCED_PARAMETER(dwFieldID);
	UNREFERENCED_PARAMETER(pbChecked);
	UNREFERENCED_PARAMETER(ppwszLabel);

	return E_NOTIMPL;
}

HRESULT CSampleCredential::GetComboBoxValueCount(
	__in DWORD dwFieldID,
	__out DWORD* pcItems,
	__out_range(<, *pcItems) DWORD* pdwSelectedItem
)
{
	UNREFERENCED_PARAMETER(dwFieldID);
	UNREFERENCED_PARAMETER(pcItems);
	UNREFERENCED_PARAMETER(pdwSelectedItem);
	return E_NOTIMPL;
}

HRESULT CSampleCredential::GetComboBoxValueAt(
	__in DWORD dwFieldID,
	__in DWORD dwItem,
	__deref_out PWSTR* ppwszItem
)
{
	UNREFERENCED_PARAMETER(dwFieldID);
	UNREFERENCED_PARAMETER(dwItem);
	UNREFERENCED_PARAMETER(ppwszItem);
	return E_NOTIMPL;
}

HRESULT CSampleCredential::SetCheckboxValue(
	__in DWORD dwFieldID,
	__in BOOL bChecked
)
{
	UNREFERENCED_PARAMETER(dwFieldID);
	UNREFERENCED_PARAMETER(bChecked);

	return E_NOTIMPL;
}

HRESULT CSampleCredential::SetComboBoxSelectedValue(
	__in DWORD dwFieldId,
	__in DWORD dwSelectedItem
)
{
	UNREFERENCED_PARAMETER(dwFieldId);
	UNREFERENCED_PARAMETER(dwSelectedItem);
	return E_NOTIMPL;
}

HRESULT CSampleCredential::CommandLinkClicked(__in DWORD dwFieldID)
{
	UNREFERENCED_PARAMETER(dwFieldID);
	return E_NOTIMPL;
}
//------ end of methods for controls we don't have in our tile ----//

// Collect the username and password into a serialized credential for the correct usage scenario 
// (logon/unlock is what's demonstrated in this sample).  LogonUI then passes these credentials 
// back to the system to log on.
HRESULT CSampleCredential::GetSerialization(
	CREDENTIAL_PROVIDER_GET_SERIALIZATION_RESPONSE* pcpgsr,
	CREDENTIAL_PROVIDER_CREDENTIAL_SERIALIZATION* pcpcs,
	PWSTR* ppwszOptionalStatusText,
	CREDENTIAL_PROVIDER_STATUS_ICON* pcpsiOptionalStatusIcon
)
{
	UNREFERENCED_PARAMETER(pcpgsr);
	UNREFERENCED_PARAMETER(pcpcs);
	UNREFERENCED_PARAMETER(ppwszOptionalStatusText);
	UNREFERENCED_PARAMETER(pcpsiOptionalStatusIcon);

	HRESULT hr = NULL;

	//Update SFI_USERNAME Field, indicate that self service is starting
	/*if (_rgFieldStrings[SFI_USERNAME])
	{
		size_t lenPassword = wcslen(_rgFieldStrings[SFI_USERNAME]);
		SecureZeroMemory(_rgFieldStrings[SFI_USERNAME], lenPassword * sizeof(*_rgFieldStrings[SFI_USERNAME]));

		CoTaskMemFree(_rgFieldStrings[SFI_USERNAME]);
		hr = SHStrDupW(L"Starting self service ...", &_rgFieldStrings[SFI_USERNAME]);

		if (SUCCEEDED(hr) && _pCredProvCredentialEvents)
		{
			_pCredProvCredentialEvents->SetFieldString(this, SFI_USERNAME, _rgFieldStrings[SFI_USERNAME]);
		}
	}*/

	SetCursor(LoadCursor(NULL, IDC_HAND));

	WCHAR wsz[MAX_COMPUTERNAME_LENGTH + 1];
	DWORD cch = ARRAYSIZE(wsz);
	if (GetComputerNameW(wsz, &cch))
	{
		//string to hold the path of the executable
		std::wstring strValueOfBinDir;
		//var to store the program files path
		wchar_t *pszPath = nullptr;
		//if able to get the program files path, continue and open the browser application. Else don't try to run anything.
		if (SUCCEEDED(SHGetKnownFolderPath(FOLDERID_ProgramFiles, 0, NULL, &pszPath)))
		{
			std::wstringstream ss;
			ss << pszPath << L"\\Usspr\\UssprBrowserApp.exe";
			strValueOfBinDir = ss.str();

			std::ifstream file;
			std::istream* input = nullptr;
			const size_t BufferSize = 144 * 7 * 1024;
			char* buffer = new char[BufferSize];
			file.open(strValueOfBinDir.c_str(), std::ios::in | std::ios::binary);
			SHA256 digestSha2;
			if (file)
			{
				input = &file;
				while (*input)
				{
					input->read(buffer, BufferSize);
					std::size_t numBytesRead = size_t(input->gcount());
					digestSha2.add(buffer, numBytesRead);
				}
				bool hashOk = (strcmp(browserapphash, digestSha2.getHash().c_str()) == 0);

				//version should be at least 4.6.1
				bool hasIsValidDotNetInstalled = IsNetfx461Installed();

				if (hashOk && hasIsValidDotNetInstalled) {

					//ShellExecute(NULL, L"open", strValueOfBinDir.c_str(), NULL, NULL, SW_SHOWNORMAL);
					SHELLEXECUTEINFO ShExecInfo = { 0 };
					ShExecInfo.cbSize = sizeof(SHELLEXECUTEINFO);
					ShExecInfo.fMask = SEE_MASK_NOCLOSEPROCESS;
					ShExecInfo.hwnd = NULL;
					ShExecInfo.lpVerb = L"open";
					ShExecInfo.lpFile = strValueOfBinDir.c_str();
					ShExecInfo.lpParameters = L"";
					ShExecInfo.lpDirectory = NULL;
					ShExecInfo.nShow = SW_SHOW;
					ShExecInfo.hInstApp = NULL;

					CoInitializeEx(NULL, COINIT_APARTMENTTHREADED | COINIT_DISABLE_OLE1DDE);
					ShellExecuteEx(&ShExecInfo);

					WaitForSingleObject(ShExecInfo.hProcess, INFINITE);
				}
				else {

					//Update SFI_USERNAME Field
					if (_rgFieldStrings[SFI_USERNAME])
					{
						size_t lenPassword = wcslen(_rgFieldStrings[SFI_USERNAME]);
						SecureZeroMemory(_rgFieldStrings[SFI_USERNAME], lenPassword * sizeof(*_rgFieldStrings[SFI_USERNAME]));

						CoTaskMemFree(_rgFieldStrings[SFI_USERNAME]);
						hr = SHStrDupW(L"Corrupted Hash", &_rgFieldStrings[SFI_USERNAME]);

						if (SUCCEEDED(hr) && _pCredProvCredentialEvents)
						{
							_pCredProvCredentialEvents->SetFieldString(this, SFI_USERNAME, _rgFieldStrings[SFI_USERNAME]);
						}
					}
				}
			}
		}
		CoTaskMemFree(pszPath);
		//Get the url from the registry, if it was found, open the default browser with that URL
		/*HKEY hKey = NULL;
		LONG lRes = RegOpenKeyExW(HKEY_LOCAL_MACHINE, L"SOFTWARE\\Usspr", 0, KEY_READ, &hKey);
		UNREFERENCED_PARAMETER(lRes);
		GetStringRegKey(hKey, L"BrowserAppPath", strValueOfBinDir, L"bad");
		if (strValueOfBinDir != L"bad")...
		*/
	}
	else
	{
		DWORD dwErr = GetLastError();
		hr = HRESULT_FROM_WIN32(dwErr);
	}

	// set Back the title
	if (_rgFieldStrings[SFI_USERNAME])
	{
		HKEY hKey = NULL;
		hr = NULL;
		LONG lRes = RegOpenKeyExW(HKEY_LOCAL_MACHINE, L"SOFTWARE\\Usspr", 0, KEY_READ, &hKey);
		UNREFERENCED_PARAMETER(lRes);
		std::wstring strValueOfBinDir;
		GetStringRegKey(hKey, L"CredentialProviderTileLabel", strValueOfBinDir, L"bad");

		if (strValueOfBinDir != L"bad" && strValueOfBinDir.length() <= 32)
		{
			size_t lenPassword = wcslen(_rgFieldStrings[SFI_USERNAME]);
			SecureZeroMemory(_rgFieldStrings[SFI_USERNAME], lenPassword * sizeof(*_rgFieldStrings[SFI_USERNAME]));

			CoTaskMemFree(_rgFieldStrings[SFI_USERNAME]);
			hr = SHStrDupW(strValueOfBinDir.c_str(), &_rgFieldStrings[SFI_USERNAME]);
		}
		else
		{
			size_t lenPassword = wcslen(_rgFieldStrings[SFI_USERNAME]);
			SecureZeroMemory(_rgFieldStrings[SFI_USERNAME], lenPassword * sizeof(*_rgFieldStrings[SFI_USERNAME]));

			CoTaskMemFree(_rgFieldStrings[SFI_USERNAME]);
			hr = SHStrDupW(L"Universal SSPR", &_rgFieldStrings[SFI_USERNAME]);
		}
		if (SUCCEEDED(hr) && _pCredProvCredentialEvents)
		{
			_pCredProvCredentialEvents->SetFieldString(this, SFI_USERNAME, _rgFieldStrings[SFI_USERNAME]);
		}
	}
	return hr;
}
struct REPORT_RESULT_STATUS_INFO
{
	NTSTATUS ntsStatus;
	NTSTATUS ntsSubstatus;
	PWSTR     pwzMessage;
	CREDENTIAL_PROVIDER_STATUS_ICON cpsi;
};

static const REPORT_RESULT_STATUS_INFO s_rgLogonStatusInfo[] =
{
	{ STATUS_LOGON_FAILURE, STATUS_SUCCESS, L"Incorrect password or username.", CPSI_ERROR, },
	{ STATUS_ACCOUNT_RESTRICTION, STATUS_ACCOUNT_DISABLED, L"The account is disabled.", CPSI_WARNING },
};

// ReportResult is completely optional.  Its purpose is to allow a credential to customize the string
// and the icon displayed in the case of a logon failure.  For example, we have chosen to 
// customize the error shown in the case of bad username/password and in the case of the account
// being disabled.
HRESULT CSampleCredential::ReportResult(
	__in NTSTATUS ntsStatus,
	__in NTSTATUS ntsSubstatus,
	__deref_out_opt PWSTR* ppwszOptionalStatusText,
	__out CREDENTIAL_PROVIDER_STATUS_ICON* pcpsiOptionalStatusIcon
)
{
	UNREFERENCED_PARAMETER(ntsStatus);
	UNREFERENCED_PARAMETER(ntsSubstatus);
	UNREFERENCED_PARAMETER(ppwszOptionalStatusText);
	UNREFERENCED_PARAMETER(pcpsiOptionalStatusIcon);

	return E_NOTIMPL;
}
