﻿Version 1.0.0.10
 - Restore lower integrity level launch of the UssprBrowserApp
 - Before navigating, now checking that the hostname is the same as the original.
 - When compiled with a public key, public key pinning is activated
 - Check connection and certificate before navigating
 - Added hook that closes any dialog that tries to show. Also closes the UssprBrowserApp when it happens.

Version 1.0.0.9
- https://gitlab.com/smartwavesa/open-source/usspr/issues/11 : more characters are now allowed in the title, including question mark
- https://gitlab.com/smartwavesa/open-source/usspr/issues/12 : the counter now always displays in the title.

Version 1.0.0.8
- Same build installs and runs on both Win7 x64 and Win10 x64

Version 1.0.0.7
- Full screen without border
- Do not display "starting self service ..."
- Can only be started at SYSTEM Integrity Level.
- Added a new parameter "CredentialProviderTileLabelExplanation" that contains the placeholder !TIMELEFT! to position the countdown.
- Now packaged using Wix version 3.11.1
- Set Product icon in add/remove view
- Safer Credential Provider compile options
- Added .Net framework detection logic before launching the browser app

Version 1.0.6
- Prevent browser context menu from displaying, even during launch.

Version 1.0.5
- Not released

Version 1.0.4
- Removed creation of a link to browserApp in the start menu folder, so that the program is not found in the search bar.
- 5 new installation parameters:
	BrowserTimeoutPerPage
	BrowserAppLogFilePath
	MaximumLogFileSize
	MaximumNumberOfLogFiles
	LogLevel
- Visual Studio 2015 (v140) and windows sdk 10.15063 dependencies are now embedded. 
	Note: .Net 4.6.1 or later is still a pre-requisite in windows 7.
	It's already installed by default with windows 10.
	cf: https://docs.microsoft.com/en-us/dotnet/framework/get-started/system-requirements	
- Allowing shift based Uppercase letter.
- Windows envirronment variable in usspr-installer-config.xml are now supported to ease installation automation.
- Timeout display is now language neutral.
- Fix inconsistent log message
- Installer windows title is now "USSPR Universal Self Service Password Windows client"


Version 1.0.3
- Only one instance of BrowserApp can be run at the same time.
- We don't use a strict white list anymore, but rather any combination of non modifiers keys and whitelisted singlekeys is allowed.  
- In order to prevent a double click in the title, the windows title bar got removed. Instead we provided a WPF title bar which we can control.
The new title bar looks like a windows 7 title bar. 
- CP update (CPv1v2):
		- Replacing NULL pointers with nullptr, there are probably more to find.
		- Display messages when tile is clicked. ( "starting self service" and "Corrupted Hash")
		- To Do: Cursor type set in waiting state.
		- To Do: There is a warning only while using sdk 14.393. Narrowing down conversion. We need to fix it or use a newer version.

Version 1.0.2
- Default image is the default USSPR logo. Not the one from VdL.
- White list implementation: 
	- Whitelisted keys are printed at startup in log.
    - Only blocked actions (Keys pressed) are logged.
    - Combination of more than 3 keys are not allowed.
    - Default white list allow numbers, letters and special characters.
- Running BrowserApp as another user: 
    - Running BrowserApp at logon as another user is not doable. Only SYSTEM account is allowed to attach to logonui.exe 
    - Therefore the only option left is to create a process runned by SYSTEM but with restricted rights.
    - Depending on his inital (it should be SYSTEM) integrity level, the BrowserApp will relaunch itself in MEDIUM integrity level.
	- No matter how you launch BrowserApp, it cannot have a higher integrity level than MEDIUM.
- Max size for CredentialProviderTileLabel : ~32 character (some character counts for more than one)
- Default timer: 300s. Time left is displayed in the title. Timer is reset when browsing to another page.
- Using visual studio 2015 i.e plateform toolset = v140. (For the 2 C++ projects of the solution).
- Windows SDK 10.0.14393.0
- BrowserApp is launched directly (as a WinExe app), no command prompt is visible. (As it would be with an Exe app).
- Various improvements to make the whitelist MUCH faster.

Version 1.0.0
- Supposed to work for windows 10
- Updated dependencies:
	WIX toolset v3.11
	Windows SDK 10.0.15063.0
	Plateform toolset: Visual Studio 2017 (v141)
- New image with the recommended siye of 192 192
- Code Updated to accept only this size
- Recalculation of the hash.

Version 0.4.1
- Fixed bug SWSSPRUNI-79 : The windows client does not uninstall properly

Version 0.4.0
- The embedded browser window does not more load if there is a certificate error (CN, CA or validity) when loading the sspr. 
- The embedded browser application install path can no more be overridden at install time.
- The embedded browser application install path can no more be changed afterwards (removed registry setting)
- The embedded browser window title is now restricted to 32 characters max, and its content is limited to a restricted set of characters.
- The credential provider tile image, when overridden, is restricted to be a 24-bit uncompressed bitmap of size 128x128.
- The credential provider now checks the browser application binary checksum before launching it.
- The URL of the SSPR web app must be provided during installation. Re-install is the only way to change it.
- When a custom tile image is provided during installation, it cannot be changed afterwards.
- The required .Net version is now 4.6.1

Version 0.3.0
- The Usspr Browser now checks the validity of the Uri found in the registry against a checksum
- Add logging to the Usspr Browser Application

Version 0.2.0
- The Credential Provider tile title can now be changed by a setup parameter. Once installed, it is stored in a registry string.
- Registry Paths, Dll files and Exe files now use the Usspr* naming.
- The Credential Provider tile image can now be overriden by a setup parameter. Once installed, it is stored in a registry string.
- The browser application nows correctly maximizes
- Now we ship two example files for the setup configuration along with the installer. (full with all possible parameters. minimal with required parameters only.)
- The browser application now displays the custom tile label (when defined) in the title bar.
- Added inactive timeout and corresponding application setting InactiveTimeoutSeconds

Version 0.1.0
- The credential provider now builds and installs on both 32-bit and 64-bit platforms
- Configuration of the installer is now done via a configuration file
- The configuration file now accepts the UssprUrl parameter
- The release number is now managed solution-wide and is reflected in the msi name and product information
