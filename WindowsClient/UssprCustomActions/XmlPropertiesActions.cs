﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Deployment.WindowsInstaller;
using System.Xml.Linq;
using System.Linq;
using Usspr.BrowserApp;
using System.DirectoryServices;
using System.Text.RegularExpressions;
using Usspr.BrowserApp.Loaders;

namespace Usspr.Msi.CustomActions
{
    public class XmlPropertiesActions
    {
        private static Regex validWindowsCommand = new Regex(@"^[a-zA-Z0-9()_-]{1,}$");
        [CustomAction]
        public static ActionResult SetInstallerProperties(Session session)
        {
            session.Log("Begin SetInstallerProperties");
            
            try
            {
                var doc = XDocument.Load(session["CFGPATH"]);

                session.Log("Parameters Loaded:" + (doc.Root != null));
                session.Log("Parameter Count:" + doc.Descendants("Parameter").Count());
                // Load parameters from XML
                var parameters = doc.Descendants("Parameter").ToDictionary(n => n.Attribute("Name").Value, v => v.Attribute("Value").Value);
                session.Log("Number of parameters loaded from setup config file: " + parameters.Count());
                SetTitleAndExplanation(ref parameters);

                if (parameters.Any())
                {
                    //Set the Wix Properties in the Session object
                    foreach (var parameter in parameters)
                    {
                        string[] parsedValue = parameter.Value.Split('%');
                        session.Log(parameter + "Length parsed Value" + parsedValue.Length);
                        // There are no environment variable
                        if (parsedValue.Length == 1)
                        {
                            session[parameter.Key] = parameter.Value;
                        }
                        else
                        {
                            // There are ${NumberOfEnvirronmentVariable} envirronment variable
                            int NumberOfEnvirronmentVariable = (parsedValue.Length - 1) / 2;
                            session.Log("NumberOfEnvirronment variable: " + NumberOfEnvirronmentVariable);

                            for (int i = 1; i < parsedValue.Length; i = i + 2)
                            {
                                session.Log("round: " + i + " replacing: " + parsedValue[i] + "...");
                                parsedValue[i] = replaceKeyByEnvirronmentalue(session, parsedValue[i]);
                                session.Log("By " + parsedValue[i]);
                            }

                            string command = "";
                            foreach (string s in parsedValue)
                            {
                                command = command + s;
                            }
                            session.Log("Finally: " + command);
                            session[parameter.Key] = command;
                        } 
                    }
                }
                else
                {
                    session.Log("No Parameters loaded");
                }
            }
            catch (Exception ex)
            {
                session.Log("ERROR in custom action SetInstallerProperties {0}", ex.ToString());
                return ActionResult.Failure;
            }
            session.Log("End SetInstallerProperties");
            return ActionResult.Success;
        }

        private static void SetTitleAndExplanation(ref Dictionary<string, string> parameters)
        {
            var titleLoader = new Title();
            var titleExplanationLoader = new TitleExplanation();
            parameters.TryGetValue("CredentialProviderTileLabel", out string titleParam);
            parameters.TryGetValue("CredentialProviderTileLabelExplanation", out string titleParamExpl);
            titleLoader.TryParseOrDefault(titleParam);
            titleExplanationLoader.TryParseOrDefault(titleParamExpl);
            parameters["CredentialProviderTileLabel"] = titleLoader.Value;
            parameters["CredentialProviderTileLabelExplanation"] = titleExplanationLoader.Value;
        }

        private static string replaceKeyByEnvirronmentalue(Session session, string key)
        {
            if (!String.IsNullOrEmpty(key) && !String.IsNullOrWhiteSpace(key) && validWindowsCommand.IsMatch(key))
            {
                session.Log("Envirronment Key: " + key);
                session.Log("Envirronment Value: " + Environment.GetEnvironmentVariable(key));
                // Making sure that the corresponding envirronment variable exist and is not empty string
                if (String.IsNullOrEmpty(Environment.GetEnvironmentVariable(key)) || String.IsNullOrEmpty(Environment.GetEnvironmentVariable(key)))
                {
                    return "%"+key+"%";
                }
                else
                {
                    return Environment.GetEnvironmentVariable(key);
                }
            }
            else
            {
                return "%" + key + "%";
            }
        }


        [CustomAction]
        public static ActionResult ComputeUriHash(Session session)
        {
            session.Log("Begin ComputeUriHash");
            try
            {
                var urlFromSessionParameter = session["UssprUrl"];
                var canonicalUri = new Uri(urlFromSessionParameter);
                String imagePath = session["CredentialProviderTileImagePath"] ?? string.Empty;
                var checksum = SsprUriRegistryLoader.Hash(canonicalUri.ToString(), imagePath);
                session["UssprUrlChecksum"] = checksum; 
            }
            catch (Exception ex)
            {
                session.Log("ERROR in custom action ComputeUriHash {0}", ex.ToString());
                return ActionResult.Failure;
            }
            session.Log("End ComputeUriHash");
            return ActionResult.Success;
        }

        [CustomAction]
        public static ActionResult CheckInstallDirNotChanged(Session session)
        {
            session.Log("Begin CheckInstallDirNotChanged");
            try
            {
                var programFilesFolderToUse = session["PlatformProgramFilesFolder"];
                session.Log("CheckInstallDirNotChanged programFilesFolderToUse : " + programFilesFolderToUse); 
                var allowedInstalledDir = session[programFilesFolderToUse] + @"Usspr\";
                var realInstallDir = session["INSTALLDIR"];
                session.Log("CheckInstallDirNotChanged allowedInstalledDir : " + allowedInstalledDir);
                session.Log("CheckInstallDirNotChanged realInstallDir : " + realInstallDir);
                if (!String.Equals(allowedInstalledDir, realInstallDir))
                {
                    session.Log("ERROR: you are not allowed to change the install directory");
                    return ActionResult.Failure;
                }
            }
            catch (Exception ex)
            {
                session.Log("ERROR in custom action CheckInstallDirNotChanged {0}", ex.ToString());
                return ActionResult.Failure;
            }
            session.Log("End CheckInstallDirNotChanged");
            return ActionResult.Success;
        }
    }
}
